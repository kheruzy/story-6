from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import *
from .models import *
from .forms import CreateStory
import json
import time

# Create your tests here.
class StoryTest(TestCase):

    def test_url_exists(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 404)

    def test_uses_view(self):
        handler = resolve(r'')
        self.assertEqual(handler.func, landingpage)

    def test_uses_template(self):
        response = self.client.get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_client_post(self):
        response = Client().post('',{'title':'judul','text':'ini ceritanya'})
        self.assertEqual(response.status_code, 404)

    def test_model_add(self):
        Story.objects.create(title='judul',text='ini ceritanya').save()
        jumlah = Story.objects.all().count()
        self.assertEqual(jumlah,1)
        self.assertNotEqual(jumlah,0)

    def test_model_delete(self):
        Story.objects.create(title='judul',text='ini ceritanya').save()
        Story.objects.get(title='judul').delete()
        jumlah = Story.objects.all().count()
        self.assertEqual(jumlah,0)
        self.assertNotEqual(jumlah,1)

    def test_story_form(self):
        form = CreateStory(data={'title':'judul','text':'ini ceritanya'})
        self.assertTrue(form.is_valid())
        t = form.cleaned_data['title']
        self.assertEqual(t,'judul')
        t1 = form.cleaned_data['text']
        self.assertEqual(t1,'ini ceritanya')

    def test_status_form_empty(self):
        form = CreateStory(data={'title':'','text':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['title'],
            ["This field is required."]
        )

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def try_adding_story(self):

